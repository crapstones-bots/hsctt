# HSCTT Bot

The HSCTT (HTTP StatusCode to text) bot was an idea we had early in the morning when 4 CS students were drunk.
In the beginning it was just a hacky telegram bot in the form of an ugly python script and
now it became a fully fledged microservice architecture with bots for multiple platforms.

## What It Does

This bot checks all sent text messages and looks for three digit numbers.
If the number is in the "database" it replies with the message (HTTP status in case of valid status code or custom message if set).

Example:

![example screenshot](assets/example_screenshot.jpg)

## Supported Commands

With the exception of `/codes`, which is usable by everyone, the following commands can only be used by the "owner" of the bot (user id can be set in the settings file).

| Command                              | Explanation                                                              |
| ------------------------------------ | ------------------------------------------------------------------------ |
| `/codes`                             | List all available codes (default and custom). Can be pinned in a group. |
| `/addcode <number> <message>`        | Add a custom code (message can contain spaces)                           |
| `/updatecode <number> <new message>` | Update the message of an existing custom code                            |
| `/deletecode <number>`               | Delete an existing custom code                                           |

## Deployment

> A Docker container with all binaries included will come in the future.

Run `cargo build --release --workspace` to build all binaries.
In `target/release` there is now a binary for every available bot and the `codes_service` which holds the database of codes.
Either copy the binaries to a server or start them in this directory.

First you have to start the `codes_service` then you can start each bot you want.
Go to the section of each bot to see how you start them.

### Telegram

First have a look at [Deployment](#deployment)

You can either use the config file `telegram.toml` (copy from [`telegram.toml.example`](https://codeberg.org/crapstones-bots/hsctt/src/branch/main/telegram.toml.example))
or set following environment variables:

```bash
# the token you get from Bot Father
TELEGRAM_TOKEN="123456789:ABCDEFGHIJKLMNOPQ_0123456789abcdefg"
# the url your codes_service listens on
SERVICE_URL="http://localhost:3030"
# your Telegram id (only the user with this id can run all commands)
CREATOR_ID=0
```

Now you can start the `telegram` binary.

### Discord

First have a look at [Deployment](#deployment)

You can either use the config file `discord.toml` (copy from [`discord.toml.example`](https://codeberg.org/crapstones-bots/hsctt/src/branch/main/discord.toml.example))
or set following environment variables:

```bash
# the token you get from Bot Father
DISCORD_TOKEN="<Discord bot token>"
# the url your codes_service listens on
SERVICE_URL="http://localhost:3030"
# your Discord id (only the user with this id can run all commands)
CREATOR_ID=0
```

Now you can start the `discord` binary.
