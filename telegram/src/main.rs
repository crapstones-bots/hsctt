use std::env;

use lazy_static::lazy_static;
use teloxide::{
    dispatching::UpdateFilterExt,
    prelude::*,
    utils::command::{BotCommands, ParseError},
};

use hsctt_lib::{
    code_finder,
    config::{env_config, read_config, Config},
    service_bridge, Code,
};

type BotResult<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

const BOT_TOKEN_NAME: &str = "TELEGRAM_TOKEN";

lazy_static! {
    // read config file and replace values with values from environment variables
    static ref CONFIG: Config<u64> = read_config("telegram.toml").map_or_else(|_| env_config(BOT_TOKEN_NAME), |c| c.merge(env_config(BOT_TOKEN_NAME))).check();
}

#[derive(BotCommands, Clone)]
#[command(rename = "lowercase", description = "these commands are available")]
pub enum OpenCommands {
    #[command(description = "list all codes")]
    Codes,
}

#[derive(BotCommands, Clone)]
#[command(
    rename = "lowercase",
    description = "these commands are only available to the creator"
)]
pub enum CreatorCommands {
    #[command(description = "add a code", parse_with = "message_to_code")]
    AddCode(Code),
    #[command(description = "update a code", parse_with = "message_to_code")]
    UpdateCode(Code),
    #[command(description = "delete a code")]
    DeleteCode(u16),
}

#[derive(Clone)]
struct ConfigParameters {
    creator_id: UserId,
}

#[tokio::main]
async fn main() {
    run().await;
}

async fn run() {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "telegram=info,hsctt_lib=info");
    }
    pretty_env_logger::init();
    log::info!("Starting hsctt_telegram_bot...");

    service_bridge::init(&CONFIG.service_url);

    let bot = Bot::new(&CONFIG.bot_token).auto_send();

    log::info!(
        "bot name: {:?}",
        bot.get_me()
            .await
            .expect("could not get information about bot")
            .user
            .full_name()
    );

    let handler = Update::filter_message()
        .branch(
            dptree::entry()
                .filter_command::<OpenCommands>()
                .endpoint(open_commands_handler),
        )
        .branch(
            dptree::filter(|msg: Message, cfg: ConfigParameters| {
                msg.from()
                    .map(|user| user.id == cfg.creator_id)
                    .unwrap_or_default()
            })
            .filter_command::<CreatorCommands>()
            .endpoint(creator_commands_handler),
        )
        .branch(
            dptree::filter(|msg: Message| msg.from().map(|user| !user.is_bot).unwrap_or_default())
                .endpoint(|msg: Message, bot: AutoSend<Bot>| async move {
                    let text = match msg.kind {
                        teloxide::types::MessageKind::Common(ref msg) => match &msg.media_kind {
                            teloxide::types::MediaKind::Animation(inner) => inner.caption.as_ref(),
                            teloxide::types::MediaKind::Audio(inner) => inner.caption.as_ref(),
                            teloxide::types::MediaKind::Document(inner) => inner.caption.as_ref(),
                            teloxide::types::MediaKind::Photo(inner) => inner.caption.as_ref(),
                            teloxide::types::MediaKind::Text(inner) => Some(&inner.text),
                            teloxide::types::MediaKind::Video(inner) => inner.caption.as_ref(),
                            teloxide::types::MediaKind::Voice(inner) => inner.caption.as_ref(),
                            _ => None,
                        },
                        _ => None,
                    };

                    if let Some(text) = text {
                        if let Some(m) = code_finder::find_codes_and_get_messages(text).await? {
                            bot.send_message(msg.chat.id, m)
                                .reply_to_message_id(msg.id)
                                .await?;
                        }
                    }
                    Ok(())
                }),
        );

    let parameters = ConfigParameters {
        creator_id: UserId(CONFIG.creator_id),
    };

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![parameters])
        .default_handler(|upd| async move {
            log::warn!("Unhandled update: {:?}", upd);
        })
        .error_handler(LoggingErrorHandler::with_custom_text(
            "An error has occurred in the dispatcher",
        ))
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;

    log::info!("Stopping bot... Goodbye!");
}

async fn open_commands_handler(
    msg: Message,
    bot: AutoSend<Bot>,
    cmd: OpenCommands,
) -> BotResult<()> {
    // TODO proper error code handling
    use OpenCommands::*;
    match cmd {
        Codes => {
            let codes = service_bridge::get_codes().await?;
            bot.send_message(
                msg.chat.id,
                serde_yaml::to_string(&codes)
                    .unwrap()
                    .strip_prefix("---\n")
                    .unwrap(),
            )
            .reply_to_message_id(msg.id)
            .await?;
        }
    }

    Ok(())
}

async fn creator_commands_handler(
    msg: Message,
    bot: AutoSend<Bot>,
    cmd: CreatorCommands,
) -> BotResult<()> {
    use CreatorCommands::*;
    match cmd {
        AddCode(code) => {
            service_bridge::add_code(&code).await?;
            bot.send_message(msg.chat.id, "code created")
                .reply_to_message_id(msg.id)
                .await?;
        }
        UpdateCode(code) => {
            service_bridge::update_code(&code).await?;
            bot.send_message(msg.chat.id, "code updated")
                .reply_to_message_id(msg.id)
                .await?;
        }
        DeleteCode(number) => {
            service_bridge::delete_code(number).await?;
            bot.send_message(msg.chat.id, "code deleted")
                .reply_to_message_id(msg.id)
                .await?;
        }
    }

    Ok(())
}

/// parser for bot commands
fn message_to_code(message: String) -> Result<(Code,), ParseError> {
    if let Some((number, message)) = message.split_once(' ') {
        let number = number
            .parse()
            .map_err(|e: std::num::ParseIntError| ParseError::IncorrectFormat(e.into()))?;
        let message = message.to_string();
        Ok((Code { number, message },))
    } else {
        Err(ParseError::Custom(
            format!("failed to parse: {}", message).into(),
        ))
    }
}
