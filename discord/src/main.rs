use std::env;

use async_trait::async_trait;
use hsctt_lib::{
    code_finder,
    config::{env_config, read_config, Config},
    service_bridge, Code,
};
use lazy_static::lazy_static;
use serenity::{
    client::{Context, EventHandler},
    framework::{
        standard::{
            macros::{command, group, hook},
            Args, CommandResult,
        },
        StandardFramework,
    },
    model::{channel::Message, prelude::Ready, Permissions},
    prelude::GatewayIntents,
    Client,
};

#[group]
#[commands(codes, addcode, updatecode, deletecode)]
struct General;

struct Handler;

const BOT_TOKEN_NAME: &str = "DISCORD_TOKEN";

lazy_static! {
    // read config file and replace values with values from environment variables
    static ref CONFIG: Config<u64> = read_config("discord.toml").map_or_else(|_| env_config(BOT_TOKEN_NAME), |c| c.merge(env_config(BOT_TOKEN_NAME))).check();
}

#[tokio::main]
async fn main() {
    run().await;
}

async fn run() {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "discord=info,hsctt_lib=info");
    }
    pretty_env_logger::init();
    log::info!("Starting hsctt_discord_bot...");

    let framework = StandardFramework::new()
        .configure(|c| c.with_whitespace(true).allow_dm(true).prefix("/"))
        .before(before)
        .after(after)
        .normal_message(normal_message)
        .group(&GENERAL_GROUP);

    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(&CONFIG.bot_token, intents)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Err creating client");

    service_bridge::init(&CONFIG.service_url);

    if let Err(why) = client.start().await {
        log::error!("Client error: {:?}", why);
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        log::info!("{} is connected!", ready.user.name);
        let permissions = Permissions::VIEW_CHANNEL | Permissions::SEND_MESSAGES;
        log::info!(
            "to invite this bot instance to your server use the following url:\n{}",
            ready.user.invite_url(ctx, permissions).await.unwrap()
        );
    }
}

#[hook]
async fn before(ctx: &Context, msg: &Message, command_name: &str) -> bool {
    if command_name == "codes" || msg.author.id.as_u64() == &CONFIG.creator_id {
        true
    } else {
        let _res = msg
            .reply(ctx, "you are not authorized to use commands")
            .await;
        false
    }
}

#[hook]
async fn after(_ctx: &Context, _msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(()) => log::debug!("Processed command '{}'", command_name),
        Err(why) => log::error!("Command '{}' returned error {:?}", command_name, why),
    }
}

#[hook]
async fn normal_message(ctx: &Context, msg: &Message) {
    log::error!("{}", &msg.content);
    if let Some(m) = code_finder::find_codes_and_get_messages(&msg.content)
        .await
        .unwrap()
    {
        if let Err(why) = msg.reply(ctx, m).await {
            log::error!("{why:?}");
        };
    }
}

#[command]
async fn addcode(_ctx: &Context, _msg: &Message, mut args: Args) -> CommandResult {
    let number = args.single::<u16>()?;
    let message = args.rest().to_string();

    service_bridge::add_code(&Code { number, message }).await?;

    Ok(())
}

#[command]
async fn updatecode(_ctx: &Context, _msg: &Message, mut args: Args) -> CommandResult {
    let number = args.single::<u16>()?;
    let message = args.rest().to_string();

    service_bridge::update_code(&Code { number, message }).await?;

    Ok(())
}

#[command]
async fn deletecode(_ctx: &Context, _msg: &Message, mut args: Args) -> CommandResult {
    let number = args.single::<u16>()?;

    service_bridge::delete_code(number).await?;

    Ok(())
}

#[command]
async fn codes(ctx: &Context, msg: &Message) -> CommandResult {
    let codes = service_bridge::get_codes().await?;

    let mut msg_buf = String::with_capacity(2000);
    let mut buf = String::new();
    let mut already_sent = false;

    msg_buf.push_str(".\n**custom codes**:");

    for (number, message) in codes.custom.into_iter() {
        buf.clear();

        buf.push_str("  ");
        buf.push_str(&number.to_string());
        buf.push_str(": ");
        buf.push_str(&message);

        if msg_buf.len() + buf.len() < 2000 {
            msg_buf.push('\n');
            msg_buf.push_str(&buf);
        } else {
            msg.channel_id.say(&ctx, &msg_buf).await?;
            already_sent = true;
            msg_buf.clear();
            msg_buf.push_str(&buf);
        }
    }

    let default_msg = [".\n**default codes**:".to_string()]
        .into_iter()
        .chain(codes.default.into_iter().map(|(number, message)| {
            let mut buf = "  ".to_string();
            buf.push_str(&number.to_string());
            buf.push_str(": ");
            buf.push_str(&message);
            buf
        }))
        .collect::<Vec<String>>()
        .join("\n");

    if already_sent || msg_buf.len() + default_msg.len() >= 2000 {
        msg.channel_id.say(&ctx, &msg_buf).await?;
        msg.channel_id.say(&ctx, &default_msg).await?;
    } else {
        msg_buf.push('\n');
        msg_buf.push_str(default_msg.strip_prefix('.').unwrap());
        msg.channel_id.say(&ctx, &msg_buf).await?;
    }

    Ok(())
}
