use std::{
    env,
    net::{IpAddr, SocketAddr},
};

use structopt::StructOpt;
use warp::Filter;

/// Provides a RESTful (more or less) web server managing some Codes.
///
/// API will be:
///
/// - `GET /codes`: return a JSON list of Codes.
/// - `POST /codes`: create a new Code.
/// - `PUT /codes/:id`: update a specific Code.
/// - `DELETE /codes/:id`: delete a specific Code.

const CODES_PATH: &str = "codes.json";

#[derive(Debug, StructOpt)]
#[structopt(name = "codes_service")]
struct Opt {
    #[structopt(short, long, default_value = "127.0.0.1")]
    ip: IpAddr,

    #[structopt(short, long, default_value = "3030")]
    port: u16,
}

#[tokio::main]
async fn main() {
    if env::var_os("RUST_LOG").is_none() {
        // Set `RUST_LOG=codes_service=debug` to see debug logs,
        // this only shows access logs.
        env::set_var("RUST_LOG", "codes_service=info");
    }
    pretty_env_logger::init();

    let opts = Opt::from_args();

    let db = models::blank_db().await;

    let api = filters::codes(db);

    // View access logs by setting `RUST_LOG=codes`.
    let routes = api.with(warp::log("codes_service"));
    // Start up the server...
    log::info!("Starting codes service...");
    warp::serve(routes)
        .run(SocketAddr::new(opts.ip, opts.port))
        .await;
}

mod filters {
    use super::handlers;
    use super::models::{Code, Db};
    use warp::Filter;

    pub fn codes(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        code_get(db.clone())
            .or(code_multiple_get(db.clone()))
            .or(codes_get(db.clone()))
            .or(code_create(db.clone()))
            .or(code_update(db.clone()))
            .or(code_delete(db))
    }

    /// GET /codes
    pub fn codes_get(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes")
            .and(warp::get())
            .and(with_db(db))
            .and_then(handlers::get_codes)
    }

    /// GET /codes/:id
    pub fn code_get(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes" / u16)
            .and(warp::get())
            .and(with_db(db))
            .and_then(handlers::get_code)
    }

    /// GET /codes with JSON body
    pub fn code_multiple_get(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes")
            .and(warp::get())
            .and(vec_codes_body())
            .and(with_db(db))
            .and_then(handlers::get_code_multiple)
    }

    /// POST /codes with JSON body
    pub fn code_create(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes")
            .and(warp::post())
            .and(code_body())
            .and(with_db(db))
            .and_then(handlers::create_code)
    }

    /// PATCH /codes/:id with JSON body
    pub fn code_update(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes" / u64)
            .and(warp::patch())
            .and(code_body())
            .and(with_db(db))
            .and_then(handlers::update_code)
    }

    /// DELETE /codes/:id
    pub fn code_delete(
        db: Db,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path!("codes" / u16)
            .and(warp::delete())
            .and(with_db(db))
            .and_then(handlers::delete_code)
    }

    fn with_db(db: Db) -> impl Filter<Extract = (Db,), Error = std::convert::Infallible> + Clone {
        warp::any().map(move || db.clone())
    }

    fn code_body() -> impl Filter<Extract = (Code,), Error = warp::Rejection> + Clone {
        warp::body::content_length_limit(1024).and(warp::body::json())
    }

    fn vec_codes_body() -> impl Filter<Extract = (Vec<u16>,), Error = warp::Rejection> + Clone {
        warp::body::content_length_limit(1024).and(warp::body::json())
    }
}

/// These are our API handlers, the ends of each filter chain.
/// Notice how thanks to using `Filter::and`, we can define a function
/// with the exact arguments we'd expect from each filter in the chain.
/// No tuples are needed, it's auto flattened for the functions.
mod handlers {
    use crate::models;

    use super::models::{Code, Db};
    use hsctt_lib::Codes;
    use std::convert::Infallible;
    use tokio::sync::RwLockReadGuard;
    use warp::http::{Response, StatusCode};

    #[inline]
    fn search_for_code(c: u16, db: &RwLockReadGuard<Codes>) -> Option<String> {
        if let Some(answer) = db.default.get(&c) {
            Some(answer.to_owned())
        } else {
            db.custom.get(&c).map(|answer| answer.to_owned())
        }
    }

    pub async fn get_codes(db: Db) -> Result<impl warp::Reply, Infallible> {
        log::debug!("get_codes");

        let codes = db.read().await;

        let codes: &Codes = &codes;

        Ok(warp::reply::with_status(
            warp::reply::json(&codes),
            StatusCode::OK,
        ))
    }

    pub async fn get_code(number: u16, db: Db) -> Result<impl warp::Reply, Infallible> {
        log::debug!("get_code: {:?}", number);

        let codes = db.read().await;

        match search_for_code(number, &codes) {
            Some(m) => Ok(warp::reply::with_status(
                Response::builder().body(m),
                StatusCode::OK,
            )),
            None => Ok(warp::reply::with_status(
                Response::builder().body("".into()),
                StatusCode::NOT_FOUND,
            )),
        }
    }

    pub async fn get_code_multiple(
        codes: Vec<u16>,
        db: Db,
    ) -> Result<impl warp::Reply, Infallible> {
        log::debug!("get_code_multiple: {:?}", codes);

        let code_db = db.read().await;

        let result: Vec<String> = codes
            .into_iter()
            .filter_map(|c| search_for_code(c, &code_db))
            .collect();

        if result.is_empty() {
            Ok(warp::reply::with_status(
                Response::builder().body("".into()),
                StatusCode::NOT_FOUND,
            ))
        } else {
            Ok(warp::reply::with_status(
                Response::builder().body(
                    serde_json::to_string(&result)
                        .expect("serializing a vector of strings should always succeed"),
                ),
                StatusCode::OK,
            ))
        }
    }

    pub async fn create_code(create: Code, db: Db) -> Result<impl warp::Reply, Infallible> {
        log::debug!("create_code: {:?}", create);

        let mut codes = db.write().await;

        if codes.default.get(&create.number).is_none() && codes.custom.get(&create.number).is_none()
        {
            codes.custom.insert(create.number, create.message);
            models::save_codes(&codes).await;
            return Ok(StatusCode::CREATED);
        }

        Ok(StatusCode::CONFLICT)
    }

    pub async fn update_code(
        id: u64,
        update: Code,
        db: Db,
    ) -> Result<impl warp::Reply, Infallible> {
        log::debug!("update_code: id={}, code={:?}", id, update);

        let mut codes = db.write().await;

        if codes.default.contains_key(&update.number) {
            return Ok(StatusCode::METHOD_NOT_ALLOWED);
        }

        if codes.custom.contains_key(&update.number) {
            let message = codes.custom.get_mut(&update.number).unwrap();
            *message = update.message;
            models::save_codes(&codes).await;
            return Ok(StatusCode::NO_CONTENT);
        }

        Ok(StatusCode::NOT_FOUND)
    }

    pub async fn delete_code(number: u16, db: Db) -> Result<impl warp::Reply, Infallible> {
        log::debug!("delete_code: id={}", number);

        let mut codes = db.write().await;

        if codes.default.contains_key(&number) {
            return Ok(StatusCode::METHOD_NOT_ALLOWED);
        }

        if codes.custom.contains_key(&number) {
            codes.custom.remove_entry(&number);
            models::save_codes(&codes).await;
            return Ok(StatusCode::NO_CONTENT);
        }

        Ok(StatusCode::NOT_FOUND)
    }
}

mod models {
    use std::{marker::PhantomData, sync::Arc};

    pub use hsctt_lib::{Code, Codes};
    use lazy_static::lazy_static;
    use tokio::{
        fs::{read, write},
        sync::{Mutex, RwLock},
    };

    use super::CODES_PATH;

    pub type Db = Arc<RwLock<Codes>>;

    pub async fn blank_db() -> Db {
        Arc::new(RwLock::new(read_codes().await))
    }

    async fn read_codes() -> Codes {
        let contents = read(CODES_PATH).await.expect("could not open codes.json");
        serde_json::from_slice(&contents).expect("failed to deserialize codes")
    }

    pub async fn save_codes(codes: &Codes) {
        lazy_static! {
            static ref WRITE_LOCK: Mutex<PhantomData<u8>> = Mutex::new(PhantomData);
        }

        let _lock = WRITE_LOCK.lock().await;
        let contents = serde_json::to_vec_pretty(codes).expect("failed to serialize codes");
        write(CODES_PATH, contents)
            .await
            .expect("could not write codes to disk");
    }
}
