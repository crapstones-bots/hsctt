import json
import time

import requests

s = requests.Session()


def print_result(res, pretty=False):
    print(f"[{res.status_code}] ", end='')
    if pretty:
        print(json.dumps(res.json(), indent=2))
    else:
        print(res.text)


print_result(s.get("http://localhost:3030/codes/666"))

time.sleep(2)

print_result(s.post("http://localhost:3030/codes", json={"number": 666, "message": "Fluttershy"}))

time.sleep(2)

print_result(s.get("http://localhost:3030/codes/666"))

time.sleep(2)

print_result(s.patch("http://localhost:3030/codes/666", json={"number": 666, "message": "Nope"}))

time.sleep(2)

print_result(s.get("http://localhost:3030/codes/666"))

time.sleep(2)

print_result(s.delete("http://localhost:3030/codes/666"))

time.sleep(2)

print_result(s.get("http://localhost:3030/codes/666"))

time.sleep(2)

print_result(s.get("http://localhost:3030/codes", json=[200, 500]))

time.sleep(2)

print_result(s.get("http://localhost:3030/codes"), pretty=True)
