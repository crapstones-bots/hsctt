use std::str::FromStr;

use serde::{de::DeserializeOwned, Deserialize};

use crate::error::Result;

#[derive(Debug, Deserialize)]
pub struct ConfigBuilder<T> {
    bot_token: Option<String>,
    service_url: Option<String>,
    creator_id: Option<T>,
}

pub struct Config<T> {
    pub bot_token: String,
    pub service_url: String,
    pub creator_id: T,
}

impl<T> ConfigBuilder<T> {
    /// Merges two `ConfigBuilder` structs where the `other` config is prioritized.
    pub fn merge(self, other: ConfigBuilder<T>) -> ConfigBuilder<T> {
        ConfigBuilder {
            bot_token: other.bot_token.or(self.bot_token),
            service_url: other.service_url.or(self.service_url),
            creator_id: other.creator_id.or(self.creator_id),
        }
    }

    /// Check if config contains `None` values and panic if one is found.
    ///
    /// # Panics
    ///
    /// Panics if `None` value is found.
    pub fn check(self) -> Config<T> {
        if self.bot_token.is_none() {
            panic!("a token must be provided via config or env variable!");
        } else if self.service_url.is_none() {
            panic!("a service url must be provided via config or env variable!");
        } else if self.creator_id.is_none() {
            panic!("a creator id must be provided via config or env variable!");
        }

        Config {
            bot_token: self.bot_token.unwrap(),
            service_url: self.service_url.unwrap(),
            creator_id: self.creator_id.unwrap(),
        }
    }
}

pub fn read_config<T: DeserializeOwned>(name: &str) -> Result<ConfigBuilder<T>> {
    let contents = std::fs::read(name)?;
    Ok(toml::from_slice(&contents)?)
}

pub fn env_config<T: FromStr>(bot_token_name: &str) -> ConfigBuilder<T> {
    let bot_token = std::env::var(bot_token_name).ok();
    let service_url = std::env::var("SERVICE_URL").ok();
    let creator_id = std::env::var("CREATOR_ID").map_or_else(|_| None, |c| c.parse().ok());

    ConfigBuilder {
        bot_token,
        service_url,
        creator_id,
    }
}
