use lazy_static::lazy_static;
use regex::{CaptureMatches, Regex};

use crate::{service_bridge, Result};

lazy_static! {
    static ref CODES_REGEX: Regex =
        Regex::new(r"(?m)(?:(?:^)|\s)(\d{3})(?:\s|(?:$)|(?:[[:punct:]]))").unwrap();
}

fn find_codes(data: &str) -> Option<CaptureMatches> {
    if CODES_REGEX.is_match(data) {
        Some(CODES_REGEX.captures_iter(data))
    } else {
        None
    }
}

pub async fn find_codes_and_get_messages(text: &str) -> Result<Option<String>> {
    // TODO find a proper fix and remove this hack!
    // (a single whitespace is consumed by the first match)
    let text = text.replace(' ', "  ");

    if let Some(matches) = find_codes(&text) {
        let codes: Vec<u16> = matches
            .map(|c| {
                c.get(1)
                    .unwrap()
                    .as_str()
                    .parse()
                    .expect("the regex only matches numbers")
            })
            .collect();

        if !codes.is_empty() {
            let code_message = if codes.len() == 1 {
                service_bridge::get_message_from_code(codes[0]).await?
            } else {
                service_bridge::get_messages_from_codes(&codes)
                    .await?
                    .map(|v| v.join("\n"))
            };

            return Ok(code_message);
        }
    }

    Ok(None)
}
