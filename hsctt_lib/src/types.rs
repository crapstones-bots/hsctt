use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Codes {
    pub custom: BTreeMap<u16, String>,
    pub default: BTreeMap<u16, String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Code {
    pub number: u16,
    pub message: String,
}
