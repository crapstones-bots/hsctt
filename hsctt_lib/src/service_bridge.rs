use lazy_static::lazy_static;
use once_cell::sync::OnceCell;
use reqwest::{Client, StatusCode};

use crate::{
    error::{HSCTTError, Result},
    Code, Codes,
};

lazy_static! {
    static ref CLIENT: Client = Client::new();
    static ref BASE_URL: OnceCell<String> = OnceCell::new();
}

pub fn init(base_url: &str) {
    let mut base_url = base_url.to_string();
    base_url.push_str(if base_url.ends_with('/') {
        "codes/"
    } else {
        "/codes/"
    });

    BASE_URL
        .set(base_url)
        .expect("service_bridge can only be initialized once!");
}

fn get_base_url() -> &'static str {
    BASE_URL
        .get()
        .expect("initialize service_bridge with base url first!")
}

pub async fn get_message_from_code(code: u16) -> Result<Option<String>> {
    let mut url = get_base_url().to_string();
    url.push_str(&code.to_string());
    let resp = CLIENT.get(&url).send().await?;

    log::info!("get single code from service: {:?}", resp.status());
    if resp.status() == StatusCode::OK {
        Ok(Some(
            resp.text()
                .await
                .expect("response should always contain a message"),
        ))
    } else {
        Ok(None)
    }
}

pub async fn get_messages_from_codes(codes: &[u16]) -> Result<Option<Vec<String>>> {
    let resp = CLIENT.get(get_base_url()).json(codes).send().await?;

    log::info!("get multiple codes from service: {:?}", resp.status());
    if resp.status() == StatusCode::OK {
        Ok(Some(
            resp.json().await.expect("deserializing should never fail"),
        ))
    } else {
        Ok(None)
    }
}

pub async fn add_code(code: &Code) -> Result<()> {
    let resp = CLIENT.post(get_base_url()).json(code).send().await?;

    log::info!("add code to service: {:?}", resp.status());
    match resp.status() {
        StatusCode::CONFLICT => Err(HSCTTError::Conflict),
        _ => match resp.error_for_status() {
            Ok(_) => Ok(()),
            Err(e) => Err(e.into()),
        },
    }
}

pub async fn update_code(code: &Code) -> Result<()> {
    let mut url = get_base_url().to_string();
    url.push_str(&code.number.to_string());
    let resp = CLIENT.patch(&url).json(code).send().await?;

    log::info!("update code from service: {:?}", resp.status());
    match resp.status() {
        StatusCode::CONFLICT => Err(HSCTTError::Conflict),
        _ => match resp.error_for_status() {
            Ok(_) => Ok(()),
            Err(e) => Err(e.into()),
        },
    }
}

pub async fn delete_code(code: u16) -> Result<()> {
    let mut url = get_base_url().to_string();
    url.push_str(&code.to_string());
    let resp = CLIENT.delete(&url).send().await?;

    log::info!("delete code from service: {:?}", resp.status());
    match resp.error_for_status() {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

pub async fn get_codes() -> Result<Codes> {
    let resp = CLIENT.get(get_base_url()).send().await?;

    log::info!("get all codes from service: {:?}", resp.status());
    if resp.status() == StatusCode::OK {
        Ok(resp.json().await.expect("deserializing should never fail"))
    } else {
        match resp.error_for_status() {
            Ok(r) => panic!("server implementation wrong (response: {:?})", r),
            Err(e) => Err(e.into()),
        }
    }
}
