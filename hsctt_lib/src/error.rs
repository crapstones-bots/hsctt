use thiserror::Error;

pub type Result<T> = std::result::Result<T, HSCTTError>;
#[derive(Debug, Error)]
pub enum HSCTTError {
    #[error("already exists")]
    Conflict,

    #[error("io error")]
    IOError(#[from] std::io::Error),

    #[error("toml error")]
    TomlError(#[from] toml::de::Error),

    #[error("request failed")]
    ReqwestError(#[from] reqwest::Error),
}
